# Phonograph Client

## How to start

1. `npm install`.
2. Start dev server usingg `ng serve`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Create docker image

```
$ docker build -t registry.gitlab.com/phonograph-bot/phonograph-frontend:X.X.X .
```

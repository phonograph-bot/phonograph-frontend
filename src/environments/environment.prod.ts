export const environment = {
  production: true,
  botId: '832223206004883497',
  apiUrl: 'https://proyectogshort.xyz/bot/api',
  apiWsUrl: 'wss://proyectogshort.xyz/',
  apiWsPath: '/bot/api/socket.io'
};

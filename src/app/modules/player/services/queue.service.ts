import { Injectable } from '@angular/core';
import { Song } from '../models/Song';
import { SocketService } from './socket.service';
import {Namespaces} from './Namespaces';
import {Observable} from 'rxjs';

@Injectable()
export class QueueService {

  constructor(private socketService: SocketService) {}

  getQueue(): Observable<Song[]>{
    return this.socketService.fromEvent<Song[]>('queue');
  }

  addQueue(url: string): void{
    return this.socketService.emit(Namespaces.Add, url);
  }
}

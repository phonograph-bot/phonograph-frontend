import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { LoginService } from 'src/app/core/login.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class SocketService extends Socket{
  constructor(private loginService: LoginService) {
    super({
      url: environment.apiWsUrl,
      options: {
        transports: ['polling' ],
        path: environment.apiWsPath,
        query: {
          token: loginService.getToken()
        }
      },
    });

    this.emit('joinServer', {});
  }
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Song} from '../models/Song';
import {environment} from '../../../../environments/environment';

@Injectable()
export class SongService {
  constructor(private httpClient: HttpClient) {}

  findSongs(title = 'music'): Observable<Song[]> {

    return this.httpClient.get<Song[]>(`${environment.apiUrl}/song`, {
      params: new HttpParams().set('name', title)
    });
  }
}

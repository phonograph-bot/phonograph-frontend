import { Injectable } from '@angular/core';
import { Namespaces } from './Namespaces';
import { Player } from '../models/ServerStatus';
import { SocketService } from './socket.service';
import { Server } from '../models/Server';

@Injectable()
export class PlayerService {

  constructor(private socketService: SocketService) { }

  getPlayerStatus$(){
    return this.socketService.fromEvent<Player>('serverStatus');
  }

  getServer$(){
    return this.socketService.fromEvent<Server>(Namespaces.Server);
  }

  pause(){
    return this.socketService.emit(Namespaces.Pause);
  }

  resume(){
    return this.socketService.emit(Namespaces.Resume);
  }

  play(){
    return this.socketService.emit(Namespaces.Play);
  }

  skip(){
    return this.socketService.emit(Namespaces.Skip);
  }

  
  setVolume(vol: number) {
    return this.socketService.emit(Namespaces.Vol, vol);
  }

  setTime(time: number) {
    return this.socketService.emit(Namespaces.Time, time);
  }
}

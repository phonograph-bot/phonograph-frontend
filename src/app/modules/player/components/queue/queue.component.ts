import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Song } from '../../models/Song';
import { QueueService } from '../../services/queue.service';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.scss']
})
export class QueueComponent implements OnInit {

  queue$!: Observable<Song[]>;

  constructor(private queueService: QueueService) { }

  ngOnInit(): void {
    this.queue$ = this.queueService.getQueue()
  }

}

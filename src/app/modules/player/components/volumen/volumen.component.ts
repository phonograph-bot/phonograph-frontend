import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatSliderChange } from '@angular/material/slider';

@Component({
  selector: 'app-volumen',
  templateUrl: './volumen.component.html',
  styleUrls: ['./volumen.component.scss']
})
export class VolumenComponent {

  @Input() vol = 50;
  @Output() volumenUpdated = new EventEmitter<number>();

  constructor() { }

  changeVolumen(volEvent: MatSliderChange) {

    if(volEvent.value) {
      this.volumenUpdated.emit(volEvent.value)
    };
  }
}

import {Component, OnInit} from '@angular/core';
import {Song} from '../../models/Song';
import {SongService} from '../../services/song.service';
import {FormControl} from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  songs: Song[] = [];
  input = new FormControl(null);
  loading = true;

  constructor(private readonly songService: SongService) { }

  ngOnInit(): void {
    this.findSongs();
    this.input.valueChanges.pipe(debounceTime(500)).subscribe((data) => {
      this.findSongs(data);
    });
  }

  findSongs(title?: string): void {
    this.loading = true;
    this.songService.findSongs(title).subscribe((songs) => {
      this.songs = songs;
      this.loading = false;
    });
  }
}

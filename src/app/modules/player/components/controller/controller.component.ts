import {BreakpointObserver} from '@angular/cdk/layout';
import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {Player} from '../../models/ServerStatus';
import {Song} from '../../models/Song';
import {PlayerService} from '../../services/player.service';
import {QueueService} from '../../services/queue.service';
import {VolumenComponent} from '../volumen/volumen.component';
import {SearchComponent} from '../search/search.component';

@Component({
  selector: 'app-controller',
  templateUrl: './controller.component.html',
  styleUrls: ['./controller.component.scss']
})
export class ControllerComponent implements OnInit {

  showModal = true;

  player?: Player;
  currentSong?: Song;

  duration = 0;
  currentTime = 0;
  vol = 50;

  constructor(
    private playerService: PlayerService,
    private queueService: QueueService,
    private dialog: MatDialog,
    private breakpointObserver: BreakpointObserver,
  ) {
  }

  ngOnInit(): void {
    this.playerService.getPlayerStatus$().subscribe(player => {
      this.player = player;
      this.currentTime = player.minute;
    });

    this.queueService.getQueue().subscribe(queue => {
      this.currentSong = queue[0];
      this.duration = this.currentSong?._minute || 0;
    });

    this.playerService.getServer$().subscribe(server => {
      this.vol = server._volume;
    });

    this.updateController();
    this.observeBreakpoint();
  }

  updateController(): void {
    setTimeout(() => {
      if (this.player?.status === 'PLAYING') {
        this.increaseTimers();
      }

      this.updateController();
    }, 1000);
  }

  pause(): void {
    this.playerService.pause();
  }

  resume(): void {
    this.playerService.resume();
  }

  play(): void {
    this.playerService.play();
  }

  skip(): void {
    this.playerService.skip();
  }

  openVolumenModal(): void {
    const component = this.dialog.open(VolumenComponent).componentInstance;
    component.vol = this.vol;
    component.volumenUpdated.subscribe((vol: number) => this.changeVolumen(vol));
  }

  changeVolumen(vol: number): void {
    this.playerService.setVolume(vol);
  }

  changeTime(time: number | null): void {
    this.playerService.setTime(time!!);
  }

  openSearchModal(): void {
    this.dialog.open(SearchComponent, {
      width: '100%',
      maxWidth: '600px',
      height: '563px',
      panelClass: 'custom-modal'
    });
  }

  private increaseTimers(): void {
    if (this.duration <= this.currentTime && this.currentTime !== 0) {
      return;
    }
    this.currentTime++;
  }

  private observeBreakpoint(): void {
    this.breakpointObserver.observe(['(min-width: 952px)']).subscribe((result) => {
      this.showModal = result.matches;
    });
  }

  getFormat(): string {
    if (this.duration >= 3600) {
      return 'H:mm:ss';
    }

    return 'mm:ss';
  }
}

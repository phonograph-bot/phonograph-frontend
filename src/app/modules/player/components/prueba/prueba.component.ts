import { Component, OnInit } from '@angular/core';
import { QueueService } from '../../services/queue.service';

@Component({
  selector: 'app-prueba',
  templateUrl: './prueba.component.html',
  styleUrls: ['./prueba.component.scss']
})
export class PruebaComponent implements OnInit {

  constructor(private queueService: QueueService) { }

  ngOnInit(): void {
    this.queueService.getQueue().subscribe(result => {
      console.log(result)
    })
  }
}

import {Component, Input} from '@angular/core';
import { Song } from '../../models/Song';
import {QueueService} from '../../services/queue.service';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-song-card',
  templateUrl: './song-card.component.html',
  styleUrls: ['./song-card.component.scss']
})
export class SongCardComponent {

  @Input()
  song!: Song;

  @Input() isQueue!: boolean;

  constructor(private readonly queueService: QueueService, private readonly snackbar: MatSnackBar) { }

  addSong(): void{
    this.queueService.addQueue(this.song._url);
  }

  delete(): void {
    this.snackbar.open('Coming soon!');
  }
}

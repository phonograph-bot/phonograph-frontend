import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../../core/login.service';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(async params => {
      if (params.code) {
        this.login(params.code);
      }
    });
  }

  redirectLogin(): void{
    this.loginService.redirectToLogin();
  }

  getInvitationLink(): string {
    return `https://discord.com/oauth2/authorize?client_id=${environment.botId}&scope=bot&permissions=8`;
  }

  private login(code: string): void{
    this.loginService.login(code)
      .then(() => {
        window.location.reload();
      })
      .catch(() => {
        alert('Invalid token');
      });
  }
}

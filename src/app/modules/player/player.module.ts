import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayerRoutingModule } from './player-routing.module';
import { ControllerComponent } from './components/controller/controller.component';
import { MenuComponent } from './layouts/menu/menu.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MenuWithoutSidenavComponent } from './layouts/menu-without-sidenav/menu-without-sidenav.component';
import { PruebaComponent } from './components/prueba/prueba.component';
import { SongCardComponent } from './components/song-card/song-card.component';
import { SocketService } from './services/socket.service';
import { QueueService } from './services/queue.service';
import { QueueComponent } from './components/queue/queue.component';
import { PlayerService } from './services/player.service';
import { VolumenComponent } from './components/volumen/volumen.component';
import { SearchComponent } from './components/search/search.component';
import {MatInputModule} from '@angular/material/input';
import {SongService} from './services/song.service';
import {ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import {MatListModule} from '@angular/material/list';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    ControllerComponent,
    ToolbarComponent,
    MenuComponent,
    MenuWithoutSidenavComponent,
    PruebaComponent,
    SongCardComponent,
    QueueComponent,
    VolumenComponent,
    SearchComponent,
    LoginComponent,
  ],
  providers: [
    SocketService,
    QueueService,
    PlayerService,
    SongService,
  ],
  imports: [
    CommonModule,
    PlayerRoutingModule,
    SharedModule,
    MatInputModule,
    MatListModule,
    ReactiveFormsModule,
    MatSnackBarModule,
  ]
})
export class PlayerModule { }

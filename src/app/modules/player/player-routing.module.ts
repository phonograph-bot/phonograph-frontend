import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PruebaComponent } from './components/prueba/prueba.component';
import { QueueComponent } from './components/queue/queue.component';
import { MenuWithoutSidenavComponent } from './layouts/menu-without-sidenav/menu-without-sidenav.component';
import { MenuComponent } from './layouts/menu/menu.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: MenuWithoutSidenavComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: QueueComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerRoutingModule { }

import { ServerStatusType } from "./ServerStatusType";

export interface Player {
  status: ServerStatusType,
  minute: number,
}
export enum ServerStatusType {
  Playing = 'PLAYING',
  Stopped = 'STOPPED',
  Paused = 'PAUSED',
  Loading = 'LOADING',
}

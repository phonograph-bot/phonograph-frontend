import { SongType } from './SongType';

export interface Song {
  _minute: number;
  _title: string;
  _type: SongType;
  _url: string;
  _author: string;
  _image: string;
}

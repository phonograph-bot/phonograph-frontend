import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../../../core/login.service';
import {MatDialog} from '@angular/material/dialog';
import {LoginComponent} from '../../components/login/login.component';

@Component({
  selector: 'app-menu-without-sidenav',
  templateUrl: './menu-without-sidenav.component.html',
  styleUrls: ['./menu-without-sidenav.component.scss']
})
export class MenuWithoutSidenavComponent implements OnInit {

  constructor(private readonly loginService: LoginService, private readonly dialog: MatDialog) {}

  ngOnInit(): void {
    if (this.loginService.isLogged()){
      return;
    }

    this.dialog.open(LoginComponent, {
      disableClose: true,
      panelClass: 'custom-modal',
      width: '100%',
      maxWidth: '600px'
    });
  }
}

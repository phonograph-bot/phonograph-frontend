import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoginService } from 'src/app/core/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private loginService: LoginService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(async params => {
      if(params.code) {
        this.login(params.code);
      }
    })
  }

  redirectLogin(){
    this.loginService.redirectToLogin();
  }

  private login(code: string){
    this.loginService.login(code)
    .then(() => {
      this.router.navigateByUrl('/')
    })
    .catch(() => {
      alert('Invalid token')
    });
  }

}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import jwt_decode from 'jwt-decode';
import {environment} from '../../environments/environment';


@Injectable()
export class LoginService {

  constructor(
    private http: HttpClient
  ) { }

  redirectToLogin(): void{
    window.location.href = `${environment.apiUrl}/auth/discord/callback`;
  }

  login(code: string): Promise<void> {
    return this.http.get<any>(`${environment.apiUrl}/auth/discord/callback`, {
      params: {
        code
      }
    })
    .toPromise()
    .then(result => {
      this.saveToken(result.token);
    });
  }

  isLogged(): boolean{
    const decodedToken = this.getDecodedToken();
    return !!(decodedToken && Date.now() <= decodedToken.exp * 1000);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }

  getDecodedToken(): any{
    const token = this.getToken();
    if (!token) {
      return null;
    }
    return jwt_decode(token);
  }

  private saveToken(token: string): void{
    localStorage.setItem('token', token);
  }

}
